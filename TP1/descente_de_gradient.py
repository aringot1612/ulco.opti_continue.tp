import numpy as np
import matplotlib.pyplot as plt
import matplotlib
matplotlib.use('TKAgg')

def f(x):
    return np.square(x[0]-3) + np.square(x[1]+1)

def df(x):
    return np.array([2*x[0] - 6 , 2*x[1] + 2 ])

def f2(x):
    return ((x[0]+-2*x[1]+3)**2) + ((4*x[0]+x[1]-1)**2)

def df2(x):
    return (2*(x[0]+-2*x[1]+3)) + 2*4 *(4*x[0]+x[1]-1)

def f3(x):
    return np.cos(((x[0]-0.5)**2)+(1/(x[0]+1))) + np.sqrt(np.exp(((x[1]-4)**2) + 1))

def descente_gradient(start_x1, start_x2, sigma, target, error):
    X1 = []
    X2 = []
    Y = []
    sigmaList = []
    c = 0.9
    x = np.array([start_x1, start_x2])

    while (f(x) > (target + error)):
        while((f(x + sigma*(-df(x)))) > (f(x) + c*sigma*np.vdot(df(x),-df(x)))) :
            sigma = sigma*c

        x = x - (sigma * df(x))

        X1.append(x[0])
        X2.append(x[1])
        Y.append(f(x))
        sigmaList.append(sigma)

    print ("After " + str(len(X1)) + " iterations, x1 = "+ str(x[0]) +", x2 = " + str(x[1]) + " and f(x) = " + str(f(x)))
    return X1, X2, Y, sigmaList

x1, x2, y, sigmaList = descente_gradient(0, 0, 1, 0, 10**-6)

plt.plot(sigmaList, color="red", label="Sigma")
plt.plot(x1, color="blue", label="x1")
plt.plot(x2, color="darkblue", label="x2")
plt.plot(y, color="orange", label="f(x)")
plt.grid(True)
plt.legend(loc='best')
plt.show()