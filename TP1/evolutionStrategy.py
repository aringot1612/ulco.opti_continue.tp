import numpy as np
import matplotlib.pyplot as plt
import matplotlib
matplotlib.use('TKAgg')

def f(x):
    return (np.square(x[0]-3) + np.square(x[1]+1))

def df(x):
    return np.array([2*x[0] - 6 , 2*x[1] + 2 ])

def f2(x):
    return ((x[0]+-2*x[1]+3)**2) + ((4*x[0]+x[1]-1)**2)

def df2(x):
    return (2*(x[0]+-2*x[1]+3)) + 2*4 *(4*x[0]+x[1]-1)

def f3(x):
    return np.cos(((x[0]-0.5)**2)+(1/(x[0]+1))) + np.sqrt(np.exp(((x[1]-4)**2) + 1))

def evolutionStrategy(a_base, b_base, sigma, target, error):
    mean = np.array([0, 0])
    cov = np.array([[1,0], [0, 1]])
    x = np.array([a_base, b_base])
    X1 = []
    X2 = []
    Y = []
    gamma = 1.2
    sigmaList = []

    while f(x) > (target + error):
        variation = np.random.multivariate_normal(mean,cov)
        y = x + sigma*variation

        if f(y) < f(x):
            x = y
            sigma = sigma*(gamma)
        else:
            sigma = sigma*(gamma)**(-1/4)

        X1.append(x[0])
        X2.append(x[1])
        Y.append(f(x))
        sigmaList.append(sigma)

    print ("After " + str(len(X1)) + " iterations, x1 = "+ str(x[0]) +", x2 = " + str(x[1]) + " and f(x) = " + str(f(x)))
    return X1, X2, Y, sigmaList

def evolutionStrategyWithReparation(a_base, b_base, sigma, target, error, maxIteration):
    mean = np.array([0, 0])
    cov = np.array([[1,0], [0, 1]])
    x = np.array([a_base, b_base])
    X1 = []
    X2 = []
    Y = []
    gamma = 1.2
    sigmaList = []

    while f(x) > (target + error) and len(Y) < maxIteration:
        variation = np.random.multivariate_normal(mean,cov)
        y = x + sigma*variation

        if y[0] < 0:
            y[0] = -y[0]
        if y[1] < 0:
            y[1] = -y[1]

        if f(y) < f(x):
            x = y
            sigma = sigma*(gamma)
        else:
            sigma = sigma*((gamma)**(-1/4))

        X1.append(x[0])
        X2.append(x[1])
        Y.append(f(x))
        sigmaList.append(sigma)

    print ("After " + str(len(X1)) + " iterations, x1 = "+ str(x[0]) +", x2 = " + str(x[1]) + " and f(x) = " + str(f(x)))
    return X1, X2, Y, sigmaList

def evolutionStrategyWithSanctionv1(a_base, b_base, sigma, target, error, maxIteration):
    mean = np.array([0, 0])
    cov = np.array([[1,0], [0, 1]])
    x = np.array([a_base, b_base])
    X1 = []
    X2 = []
    Y = []
    gamma = 1.2
    sigmaList = []
    sanction = 0

    while f(x) > (target + error) and len(Y) < maxIteration :
        variation = np.random.multivariate_normal(mean,cov)
        y = x + sigma*variation

        if y[0] < 0 or y[1] < 0:
            sanction = 9999999999
        else :
            sanction = 0

        if f(y) + sanction < f(x):
            x = y
            sigma = sigma*gamma
        else:
            sigma = sigma*((gamma)**(-1/4))

        X1.append(x[0])
        X2.append(x[1])
        Y.append(f(x))
        sigmaList.append(sigma)

    print ("After " + str(len(X1)) + " iterations, x1 = "+ str(x[0]) +", x2 = " + str(x[1]) + " and f(x) = " + str(f(x)))
    return X1, X2, Y, sigmaList

def evolutionStrategyWithSanctionv2(a_base, b_base, sigma, target, error, maxIteration):
    mean = np.array([0, 0])
    cov = np.array([[1,0], [0, 1]])
    x = np.array([a_base, b_base])
    X1 = []
    X2 = []
    Y = []
    gamma = 1.2
    sigmaList = []

    while f(x) > (target + error) and len(Y) < maxIteration :
        variation = np.random.multivariate_normal(mean,cov)
        y = x + sigma*variation

        sanction = min(0,y[0])**2 + min(0,y[1])**2

        if f(y) + sanction < f(x):
            x = y
            sigma = sigma*gamma
        else:
            sigma = sigma*(gamma)**(-1/4)

        X1.append(x[0])
        X2.append(x[1])
        Y.append(f(x))
        sigmaList.append(sigma)

    print ("After " + str(len(X1)) + " iterations, x1 = "+ str(x[0]) +", x2 = " + str(x[1]) + " and f(x) = " + str(f(x)))
    return X1, X2, Y, sigmaList

x1, x2, y, sigmaList = evolutionStrategyWithSanctionv2(0, 0, 1, 0, 10**-6, 200)
plt.plot(sigmaList, color="red", label="Sigma")
#plt.plot(x1, color="blue", label="x1")
#plt.plot(x2, color="darkblue", label="x2")
plt.plot(y, color="orange", label="f(x)")
plt.grid(True)
plt.legend(loc='best')
plt.show()