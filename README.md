# Optimisation continue

## <br> Developper

- Arthur Ringot

## <br> Avancement

### <br> Question 1 : 
<br>

**Fonction :**
f(x) = (x1 - 3)^2 + (x2 + 1)^2

**Fonction dérivée :**
f'(x) = 2*(x1 - 3) + 2*(x2 + 1)
f'(x) = 2x_1 - 6 + 2x_2 + 2

**Gradient :**
    { 2x - 6 }
    { 2x + 2 }

### <br> Question 2 : 

**Voir code associé dans descente_de_gradient.py.**

### <br> Question 3 : 

#### <br> Gradient : 
<br>![Graphique : Gradient](TP1/images/graph_descente_de_gradient.png)
<br>![Sortie console : Gradient](TP1/images/descente_de_gradient.png)

### <br> Question 4 :
F3 me semble bien plus simple à minimiser.

## <br> Les autres algorithmes implémentés :
**Voir code associé dans evolutionStrategy.py**

### <br> Evolution Strategy 1 + 1 ES : 
#### <br> 1 + 1 ES Sans contrainte
<br>![Graphique : 1 + 1 ES Sans contrainte](TP1/images/graph_1_plus_1_es_no_constraint.png)
<br>![Sortie console : 1 + 1 ES Sans contrainte](TP1/images/1_plus_1_es_no_constraint.png)

#### <br> 1 + 1 ES Avec contrainte - réparation
<br>![Graphique : 1 + 1 ES Avec contrainte - réparation](TP1/images/graph_1_plus_1_es_constraint_reparation.png)
<br>![Sortie console : 1 + 1 ES Avec contrainte - réparation](TP1/images/1_plus_1_es_constraint_reparation.png)

#### <br> 1 + 1 ES Avec contrainte - pénalisation type 1
<br>![Graphique : 1 + 1 ES Avec contrainte - pénalisation type 1](TP1/images/graph_1_plus_1_es_constraint_sanction_v1.png)
<br>![Sortie console : 1 + 1 ES Avec contrainte - pénalisation type 1](TP1/images/1_plus_1_es_constraint_sanction_v1.png)

#### <br> 1 + 1 ES Avec contrainte - pénalisation type 2
<br>![Graphique : 1 + 1 ES Avec contrainte - pénalisation type 2](TP1/images/graph_1_plus_1_es_constraint_sanction_v2.png)
<br>![Sortie console : 1 + 1 ES Avec contrainte - pénalisation type 2](TP1/images/1_plus_1_es_constraint_sanction_v2.png)